// Parts of this file are copyrighted, or derivitive of code copyrighted 2018-2022 by the Deno authors.
// Use was granted to me under the [MIT License](https://github.com/denoland/deno/blob/main/LICENSE.md)

// THIS is Deno.
// Todo: if this isn't Deno (e.g. the browser), emulate these functions.

/** [deno:core/ops.rs#L112-L119](https://github.com/denoland/deno/blob/a27acbc2ec63dd684cf57990b30d757cd0477d9b/core/ops.rs#L112-L119) */
export type OpError = {
  $err_class_name: string
  message: string
  code: number | null
}

/** [deno:core/ops.rs#L95-L98](https://github.com/denoland/deno/blob/a27acbc2ec63dd684cf57990b30d757cd0477d9b/core/ops.rs#L95-L98) */
export type OpResult<T> = T | OpError

/** [deno:core/resources.rs#L89-L94](https://github.com/denoland/deno/blob/8d82ba729937baf83011354242cabc3d50c13dc2/core/resources.rs#L89-L94) */
export type ResourceId = number

/** [deno:ext/ffi/lib.rs#L241-L263](https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/ext/ffi/lib.rs#L241-L263) */
export type NativeType =
  | 'bool'
  | 'buffer'
  | 'f32'
  | 'f64'
  | 'function'
  | 'i16'
  | 'i32'
  | 'i64'
  | 'i8'
  | 'isize'
  | 'pointer'
  | 'u16'
  | 'u32'
  | 'u64'
  | 'u8'
  | 'usize'
  | 'void'

/** https://github.com/denoland/deno/blob/eab66a9f4da4e166df6f2831cd0141566cd7fc45/ext/ffi/lib.rs#L503-L514 */
type ForeignFunction = RegisterCallbackArgs & {
  /** Name of the symbol, defaults to the key name in symbols object. */
  name?: string
  /** When true, function calls will run on a dedicated blocking thread and will return a Promise resolving to the `result`. */
  nonblocking?: boolean
  /** When true, function calls can safely callback into JS or trigger a GC event. Default is `false`. */
  callback?: boolean
}

/** https://github.com/denoland/deno/blob/eab66a9f4da4e166df6f2831cd0141566cd7fc45/ext/ffi/lib.rs#L531-L536 */
type ForeignSymbol = ForeignFunction

/** https://github.com/denoland/deno/blob/eab66a9f4da4e166df6f2831cd0141566cd7fc45/ext/ffi/lib.rs#L538-L542 */
type FfiLoadArgs = {
  path: string
  symbols: { [name: string]: ForeignSymbol }
}

/** https://github.com/denoland/deno/blob/eab66a9f4da4e166df6f2831cd0141566cd7fc45/ext/ffi/lib.rs#L1743-L1747 */
type RegisterCallbackArgs = {
  parameters: NativeType[]
  result: NativeType
}

// This actually is different from Deno.OpenOptions - lack of `mode` which was moved to `OpenArgs`
/** https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/runtime/ops/fs.rs#L111-L121 */
export type OpenOptions = {
  append?: boolean
  create?: boolean
  createNew?: boolean
  read?: boolean
  truncate?: boolean
  write?: boolean
}

/** [deno:runtime/ops/fs.rs#L1776-L1782](https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/runtime/ops/fs.rs#L1776-L1782) */
export type MakeTempArgs = {
  dir?: string
  prefix?: string
  suffix?: string
}

/** https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/runtime/ops/permissions.rs#L25-L33 */
export type PermissionArgs = {
  name: 'read' | 'write' | 'net' | 'env' | 'sys' | 'run' | 'ffi' | 'hrtime'
  path?: string
  host?: string
  variable?: string
  kind?: string
  command?: string
}

/**
 * [deno:core/ops_builtin_v8.rs#L405-L410](https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/core/ops_builtin_v8.rs#L405-L410)
 */
export type SerializeDeserializeOptions = {
  host_objects?: unknown[]
  transferred_array_buffers?: unknown[]
}

export type PermissionState = 'granted' | 'prompt' | 'denied'

// The following code (until the end of file) is copyrighted 2022 by me.

export type Panic = never

/** Warning: this symbol is not yet stable! But I do plan on codifying it eventually somehow. */
export type __SupportedCompressionFormat = 'deflate' | 'deflate-raw' | 'gzip'

const ops = (
  Deno as unknown as {
    core: {
      ops: {
        op_base64_atob: (data: string) => OpResult<string>
        op_base64_decode: (data: string) => OpResult<Uint8Array>
        op_close: (rid: ResourceId) => OpResult<void>
        op_compression_finish: (rid: ResourceId) => OpResult<Uint8Array>
        op_compression_new: (
          format: __SupportedCompressionFormat,
          decoder?: boolean,
        ) => ResourceId | Panic
        op_compression_write: (
          rid: ResourceId,
          chunk: BufferSource,
        ) => OpResult<Uint8Array>
        op_decode: (buf: BufferSource) => OpResult<string>
        op_deserialize: (
          buf: BufferSource,
          options?: SerializeDeserializeOptions,
        ) => any
        op_encode: (text: string) => OpResult<Uint8Array>
        op_exit: () => never
        op_ffi_load: (
          args: FfiLoadArgs,
        ) => OpResult<[ResourceId, { [K: string]: Function }]>
        op_fsync_sync: (rid: ResourceId) => OpResult<void>
        op_get_env: (key: string) => OpResult<string | null>
        op_getgid: () => number | null
        op_getuid: () => number | null
        op_make_temp_file_sync: (args: MakeTempArgs) => OpResult<string>
        op_network_interfaces: () => Deno.NetworkInterfaceInfo[]
        op_open_sync: (
          path: string,
          options?: OpenOptions,
          mode?: number,
        ) => OpResult<ResourceId>
        op_query_permission: (args: PermissionArgs) => OpResult<PermissionState>
        op_remove_sync: (path: string, recursive?: boolean) => OpResult<void>
        op_serialize: (
          value: any,
          options?: SerializeDeserializeOptions,
          error_callback?: (message: string) => void,
        ) => Uint8Array
        op_set_exit_code: (code: number) => void
        op_system_memory_info: () => Deno.SystemMemoryInfo
        op_try_close: (rid: ResourceId) => void
        op_write_sync: (rid: ResourceId, data: BufferSource) => OpResult<number>
      }
    }
  }
).core.ops

export const base64_atob = ops.op_base64_atob

export const base64_decode = ops.op_base64_decode

/**
 * Close the given resource ID (rid) which has been previously opened, such as via opening or creating a file.
 * Closing a file when you are finished with it is important to avoid leaking resources.
 */
export const close = ops.op_close

/**
 * Closes the specified `compression` resource and returns any remaining transformed data left over in its buffer.
 * 
 * [deno:ext/web/compression.rs#L111-L128](https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/ext/web/compression.rs#L111-L128)
 */
export const compression_finish = ops.op_compression_finish

/**
 * Opens a new `compression` resource and returns its resource id.
 * Decoder defaults to false (initializes an encoder / compressor).
 *
 * Compression level cannot (currently) be modified. It is always [set to 6](https://docs.rs/flate2/latest/src/flate2/lib.rs.html#192-196).
 *
 * ⚠️ [Will panic](https://github.com/denoland/deno/blob/eab66a9f4da4e166df6f2831cd0141566cd7fc45/ext/web/compression.rs#L61) if specified compression format isn't supported.
 *
 * [deno:ext/web/compression.rs#L41-L65](https://github.com/denoland/deno/blob/eab66a9f4da4e166df6f2831cd0141566cd7fc45/ext/web/compression.rs#L41-L65)
 */
export const compression_new = ops.op_compression_new

/**
 * Writes the entire buffer to the specified compressor and returns the resulting transformed data as a new `Uint8Array`.
 *
 * Compression encoders (compressors) may have additional data to be read after your final call to this method.
 * Use `compression_finish` to read this data.
 * 
 * [deno:ext/web/compression.rs#L67-L109](https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/ext/web/compression.rs#L67-L109)
 */
export const compression_write = ops.op_compression_write

/**
 * Returns a string containing text decoded from the buffer passed as a parameter.
 *
 * [deno:core/ops_builtin_v8.rs#L239-L266](https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/core/ops_builtin_v8.rs#L239-L266)
 */
export const decode = ops.op_decode

/**
 * Deserializes values from data written with `serialize()`, or a compatible implementation.
 * 
 * Deno function implementation [deno:core/ops_builtin_v8.rs#L499-L568](https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/core/ops_builtin_v8.rs#L499-L568)
 * 
 * Rust [`Delegate`](https://v8docs.nodesource.com/node-12.0/d8/dbc/classv8_1_1_value_serializer_1_1_delegate.html) implementation [deno:core/ops_builtin_v8.rs#L273-L345](https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/core/ops_builtin_v8.rs#L273-L345)
 * 
 * [v8 documentation](https://v8docs.nodesource.com/node-12.0/dc/db1/classv8_1_1_value_deserializer.html)
 * 
 * [v8 C++ implementation](https://github.com/v8/v8/blob/3b15d950eeddcf6dba091b611e1a6cc100e2edd9/src/value-serializer.cc#L895-L1951)
 */
export const deserialize = ops.op_deserialize

/**
 * Takes a string as input, and returns a [Uint8Array](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Uint8Array) containing UTF-8 encoded text.
 *
 * [deno:core/ops_builtin_v8.rs#L222-L237](https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/core/ops_builtin_v8.rs#L222-L237)
 */
export const encode = ops.op_encode

/**
 * Terminates the current process with the last exit code set via `set_exit_code()`.
 *
 * This function will never return and will immediately terminate the current process.
 * The exit code is passed through to the underlying OS and will be available for consumption by another process.
 *
 * Note: On most Unix-like platforms, only the eight least-significant bits are considered.
 *
 * [deno:runtime/ops/os.rs#L128-L132](https://github.com/denoland/deno/blob/eab66a9f4da4e166df6f2831cd0141566cd7fc45/runtime/ops/os.rs#L128-L132)
 * 
 * [rust:std/process.rs.html#2048-2052](https://doc.rust-lang.org/stable/src/std/process.rs.html#2048-2052)
 */
export const exit = ops.op_exit

export const ffi_load = ops.op_ffi_load

export const fsync_sync = ops.op_fsync_sync

export const get_env = ops.op_get_env

/**
 * Returns the group id of the process on POSIX platforms. Returns `null` on Wimbows.
 * 
 * Requires `allow-sys` permission.
 * 
 * [deno:runtime/ops/os.rs#L246-L264](https://github.com/denoland/deno/blob/b4b4e5980be5e49211673b4d01c9f7d04f8572d4/runtime/ops/os.rs#L246-L264)
 */
export const get_gid = ops.op_getgid

/**
 * Returns the user id of the Deno process on POSIX platforms. Returns `null` on Wimbows.
 * 
 * Requires `allow-sys` permission.
 * 
 * [deno:runtime/ops/os.rs#L266-L284](https://github.com/denoland/deno/blob/b4b4e5980be5e49211673b4d01c9f7d04f8572d4/runtime/ops/os.rs#L266-L284)
 */
export const get_uid = ops.op_getuid

/**
 * Synchronously creates a new temporary file in the default directory for temporary files, unless `dir` is specified.
 * Other optional options include prefixing and suffixing the directory name with `prefix` and `suffix` respectively.
 *
 * The full path to the newly created file is returned.
 *
 * Multiple programs calling this function simultaneously will create different files.
 * It is the caller's responsibility to remove the file when no longer needed.
 *
 * Requires `allow-write` permission.
 * 
 * [deno:runtime/ops/fs.rs#L1847-L1874](https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/runtime/ops/fs.rs#L1847-L1874)
 */
export const make_temp_file_sync = ops.op_make_temp_file_sync

/**
 * Returns an array of the network interface information.
 * 
 * Requires `allow-sys` permission.
 * 
 * [deno:runtime/ops/os.rs#L160-L167](https://github.com/denoland/deno/blob/b4b4e5980be5e49211673b4d01c9f7d04f8572d4/runtime/ops/os.rs#L160-L167)
 */
export const network_interfaces = ops.op_network_interfaces

/**
 * Open a file and return its resource id.
 * The file does not need to previously exist if using the `create` or `createNew` open options.
 * It is the callers responsibility to close the file when finished with it.
 *
 * Requires `allow-read` and/or `allow-write` permissions depending on options.
 *
 * [deno:runtime/ops/fs.rs#L182-L197](https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/runtime/ops/fs.rs#L182-L197)
 */
export const open_sync = ops.op_open_sync

/**
 * [deno:runtime/ops/permissions.rs#L35-L67](https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/runtime/ops/permissions.rs#L35-L67)
 */
export const query_permission = ops.op_query_permission

/**
 * Synchronously removes the named file or directory.
 *
 * Requires `allow-write` permission.
 *
 * [deno:runtime/ops/fs.rs#L734-L779](https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/runtime/ops/fs.rs#L734-L779)
 */
export const remove_sync = ops.op_remove_sync

/**
 * Value serialization compatible with the HTML structured clone algorithm. The format is backward-compatible (i.e. safe to store to disk).
 *
 * Deno function implementation [deno:core/ops_builtin_v8.rs#L412-L497](https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/core/ops_builtin_v8.rs#L412-L497)
 *
 * Rust [`Delegate`](https://v8docs.nodesource.com/node-12.0/d8/dbc/classv8_1_1_value_serializer_1_1_delegate.html) implementation [deno:core/ops_builtin_v8.rs#L273-L345](https://github.com/denoland/deno/blob/378e6a8c0369f3256cde8a595d3dbdfe4f1dc2f9/core/ops_builtin_v8.rs#L273-L345)
 *
 * [v8 documentation](https://v8docs.nodesource.com/node-12.0/df/d48/classv8_1_1_value_serializer.html)
 *
 * [v8 C++ implementation](https://github.com/v8/v8/blob/3b15d950eeddcf6dba091b611e1a6cc100e2edd9/src/value-serializer.cc#L150-L893)
 */
export const serialize = ops.op_serialize

/**
 * [deno:runtime/ops/os.rs#L160-L167](https://github.com/denoland/deno/blob/b4b4e5980be5e49211673b4d01c9f7d04f8572d4/runtime/ops/os.rs#L160-L167)
 */
export const set_exit_code = ops.op_set_exit_code

/**
 * Displays the total amount of free and used physical and swap memory in the system, as well as the buffers and caches used by the kernel.
 * 
 * This is similar to the `free` command in Linux.
 * 
 * Requires `allow-sys` permission.
 * 
 * [deno:runtime/ops/os.rs#L226-L244](https://github.com/denoland/deno/blob/eab66a9f4da4e166df6f2831cd0141566cd7fc45/runtime/ops/os.rs#L226-L244)
 */
export const system_memory_info = ops.op_system_memory_info

/**
 * Try to remove a resource from the resource table.
 * If there is no resource with the specified `rid`, this is a no-op.
 *
 * https://github.com/denoland/deno/blob/eab66a9f4da4e166df6f2831cd0141566cd7fc45/core/ops_builtin.rs#L76-L88
 */
export const try_close = ops.op_try_close

export const write_sync = ops.op_write_sync
