import { OpResult } from './ops.ts'

/**
 * Unwraps Deno's low-level op call results into JavaScript level types and exceptions.
 * Doesn't replicate exactly how Deno's CLI API does it - but I think that's impossible.
 *
 * https://github.com/denoland/deno/blob/eab66a9f4da4e166df6f2831cd0141566cd7fc45/core/01_core.js#L142-L159
 *
 * https://v8.dev/docs/stack-trace-api#stack-trace-collection-for-custom-exceptions
 */
const unwrapOpResult = function <T>(result: OpResult<T>) {
  // @ts-ignore: aaaaaAAAAAAAAAAAAAAA
  if (result && result.$err_class_name !== undefined) {
    // @ts-ignore: aaaaaAAAAAAAAAAAAAAA
    const className = result.$err_class_name
    const errorBuilder =
      // @ts-ignore: aaaaaAAAAAAAAAAAAAAA
      globalThis.hasOwnProperty(className) &&
      // @ts-ignore: aaaaaAAAAAAAAAAAAAAA
      typeof globalThis[className].captureStackTrace === 'function'
        ? // @ts-ignore: aaaaaAAAAAAAAAAAAAAA
          globalThis[className]
        : // @ts-ignore: aaaaaAAAAAAAAAAAAAAA
        Deno.core.hasOwnProperty(className) &&
          // @ts-ignore: aaaaaAAAAAAAAAAAAAAA
          typeof Deno.core[className].captureStackTrace === 'function'
        ? // @ts-ignore: aaaaaAAAAAAAAAAAAAAA
          Deno.core[className]
        : // @ts-ignore: aaaaaAAAAAAAAAAAAAAA
        Deno.errors.hasOwnProperty(className) &&
          // @ts-ignore: aaaaaAAAAAAAAAAAAAAA
          typeof Deno.errors[className].captureStackTrace === 'function'
        ? // @ts-ignore: aaaaaAAAAAAAAAAAAAAA
          Deno.errors[className]
        : Error
    // @ts-ignore: aaaaaAAAAAAAAAAAAAAA
    const err = new errorBuilder(result.message)
    // @ts-ignore: aaaaaAAAAAAAAAAAAAAA
    err.name = result.$err_class_name
    // @ts-ignore: aaaaaAAAAAAAAAAAAAAA
    if (result.code) err.code = result.code
    errorBuilder.captureStackTrace(err, unwrapOpResult)
    throw err
  }
  return result as T
}

export default unwrapOpResult
